#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include <random>

enum class GameState {
    waiting,
    playing,
    gameOver
};

class FlappyBird {
        sf::RenderWindow window;

        sf::Texture backgroundImage;
        sf::Sprite background;

        sf::Texture birdImage;
        sf::Sprite bird;
        sf::Vector2i birdSize{34, 24};
        float gravity{0};
        int birdFrame{0};
        sf::Clock nextBirdTextureClock{};
        sf::Clock gameOverClock{};

        sf::Texture pipeImage;
        sf::Sprite pipeBottom;
        sf::Sprite pipeTop;
        std::vector<sf::Sprite> pipes;

        int score{0};

        sf::Font font;
        sf::Text scoreText;
        sf::Text startText;

        void moveBird();
        void movePipes();
        void checkCollision();
        void updateScore();

        void resetGame();
        void actionImpulse();

        GameState gameState{GameState::waiting};

        // show a pipe every 150 frames (at 60 frames/s it's 2.5 second)
        int nextPipeCounter{150};
        // randomize the vertical position of the pipes
        std::mt19937 randomGenerator;
        std::uniform_int_distribution<> pipeRandomY;

        void draw();
        void events();
    public:
        FlappyBird();
        void run();
};
