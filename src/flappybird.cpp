#include "flappybird.h"

#include <algorithm>

FlappyBird::FlappyBird():
    // initialize the random generator with the range [75, 350]
    randomGenerator((std::random_device())()),
    pipeRandomY(75, 350)
{
    window.create(
        sf::VideoMode(1000,600),
        "FlappyBird",
        sf::Style::Titlebar | sf::Style::Close
    );
    window.setFramerateLimit(60);
    window.setPosition(sf::Vector2i(0, 0));

    backgroundImage.loadFromFile("./resources/background.png");
    background.setTexture(backgroundImage);

    birdImage.loadFromFile("./resources/bird.png");
    bird.setTexture(birdImage);
    bird.setScale(2.f, 2.f);
    bird.setTextureRect(sf::IntRect(0, 0, birdSize.x, birdSize.y));

    pipeImage.loadFromFile("./resources/pipe.png");
    pipeBottom.setTexture(pipeImage);
    pipeBottom.setScale(1.5, 1.5);
    pipeTop.setTexture(pipeImage);
    pipeTop.setScale(1.5, -1.5); // -1.5 rotates the texture

    font.loadFromFile("./resources/flappybird.ttf");

    scoreText.setFont(font);
    scoreText.setString(std::to_string(score));
    scoreText.setPosition(10.f, 10.f);
    scoreText.setCharacterSize(50);
    scoreText.setOutlineThickness(3);

    startText.setFont(font);
    startText.setString("Press SPACE to start");
    startText.setPosition(200, 400);
    startText.setCharacterSize(50);
    startText.setOutlineThickness(3);

    resetGame();
}

void FlappyBird::resetGame() {
    pipes.clear();
    gravity = 0;
    bird.setPosition(
        window.getSize().x / 2 - birdSize.x / 2,
        window.getSize().y / 2 - birdSize.y / 2
    );
    bird.setRotation(0);
    score = 0;
    scoreText.setString(std::to_string(score));
}

void FlappyBird::moveBird() {
    if (gameState == GameState::waiting) {
        // float up and down
        if (nextBirdTextureClock.getElapsedTime().asMilliseconds() > 1000) {
            gravity = gravity == 0 ? 0.5 : -gravity;
            nextBirdTextureClock.restart();
        }
        bird.move(0, gravity);
        return;
    }
    if (bird.getPosition().y <= 520) {
        int rotation = bird.getRotation();
        if (rotation < 90 || rotation >= 330) {
            bird.rotate(2);
        }

        bird.move(0, gravity);
    }
    gravity += 0.5;

    // change the bird's texture three time per second
    if (nextBirdTextureClock.getElapsedTime().asMilliseconds() > 300) {
        bird.setTextureRect(sf::IntRect(birdFrame * birdSize.x, 0, birdSize.x, birdSize.y ));
        birdFrame = (birdFrame + 1) % 3;
        nextBirdTextureClock.restart();
    }
}

void FlappyBird::movePipes() {
    if (gameState != GameState::playing) {
        return;
    }
    // add a pipe every 150 frames
    nextPipeCounter--;
    if (nextPipeCounter == 0) {
        nextPipeCounter = 150;

        int yPos = pipeRandomY(randomGenerator); // y in range [75, 350]
        pipeBottom.setPosition(1000, yPos + 220); // leave a vertical gap of 220
        pipes.push_back(pipeBottom);
        pipeTop.setPosition(1000, yPos);
        pipes.push_back(pipeTop);
    }

    // delete the pipes that have moved out of the screen
    pipes.erase(
        std::remove_if(pipes.begin(), pipes.end(),
            [](const auto& p) {return p.getPosition().x < -100;}),
        pipes.end());

    // move the pipes to the left
    std::for_each(pipes.begin(), pipes.end(),
        [](auto& p) {p.move(-4, 0);});
}

void FlappyBird::checkCollision() {
    if (gameState != GameState::playing) {
        return;
    }
    if (std::any_of(pipes.begin(), pipes.end(),
            [this](const auto& p) {
                return p.getGlobalBounds().intersects(bird.getGlobalBounds());})) {
        gameState = GameState::gameOver;
    }
    if (bird.getPosition().y > 520) {
        gameState = GameState::gameOver;
    }
    if (gameState == GameState::gameOver) {
        gameOverClock.restart();
    }
}

void FlappyBird::updateScore() {
    if (gameState != GameState::playing) {
        return;
    }
    if (std::any_of(pipes.begin(), pipes.end(),
            [this](const auto& p) {
                return p.getPosition().x == 448;})) {
        score++;
        scoreText.setString(std::to_string(score));
    }
}

void FlappyBird::draw() {
    window.clear(sf::Color::Black);
    window.draw(background);
    for (const auto& pipe: pipes) {
        window.draw(pipe);
    }
    window.draw(bird);
    window.draw(scoreText);
    if (gameState != GameState::playing) {
        window.draw(startText);
    }
    window.display();
}

void FlappyBird::actionImpulse() {
    if (gameState == GameState::waiting) {
        gameState = GameState::playing;
    } else if (gameState == GameState::gameOver) {
        if (gameOverClock.getElapsedTime().asMilliseconds() > 500) {
            resetGame();
            gameState = GameState::playing;
        }
    } else if (gameState == GameState::playing) {
        // go (and look) up when the mouse is pressed
        bird.setRotation(-30);
        gravity = -8;
    }
}

void FlappyBird::events() {
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
        actionImpulse();
    }

    sf::Event event;
    while (window.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            window.close();
        } else if (event.type == sf::Event::KeyPressed) {
            if (event.key.code == sf::Keyboard::Escape) {
                window.close();
            } else if (event.key.code == sf::Keyboard::Space) {
                actionImpulse();
            }
        }
    }
}

void FlappyBird::run(){
    while(window.isOpen()){
        events();
        moveBird();
        movePipes();
        checkCollision();
        updateScore();
        draw();
    }
}
