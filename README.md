# Learning modern C++ and SFML with Flappy Bird

- Based on [How to Make Flappy Bird with C++](https://terminalroot.com/how-to-make-flappy-bird-with-cpp/).

## Compile

```sh
cd build
cmake ..
make
```

## Topics

### Random numbers

- [Pseudo-random number generation](https://en.cppreference.com/w/cpp/numeric/random)
- [How to generate a random number in C++?](https://stackoverflow.com/questions/13445688/how-to-generate-a-random-number-in-c)
- [How to use a random generator as a class member in C++11](https://stackoverflow.com/questions/29580865/how-to-use-a-random-generator-as-a-class-member-in-c11)

### Modifying the pipes vector

- [`std::remove_if`](https://en.cppreference.com/w/cpp/algorithm/remove)
- [`std::for_each`](https://en.cppreference.com/w/cpp/algorithm/for_each)
